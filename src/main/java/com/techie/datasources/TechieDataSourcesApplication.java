package com.techie.datasources;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TechieDataSourcesApplication {

	public static void main(String[] args) {
		SpringApplication.run(TechieDataSourcesApplication.class, args);
	}
}
