package com.techie.datasources.config;

import java.util.HashMap;
import java.util.Map;
import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;

@Configuration
@EnableJpaRepositories(
        entityManagerFactoryRef = "postgresqlEntityManagerFactory",
        transactionManagerRef = "postgresqlTransactionManager",
        basePackages = {"com.fravega.garexloader.repository.postgresql"})
public class PostgresqlConfig {

    @Value("${postgresql.jpa.hibernate.physical_naming_strategy}")
    private String namingStrategy;

    @Value("${postgresql.jpa.properties.hibernate.dialect}")
    private String dialect;

    @Bean
    @Primary
    @ConfigurationProperties(prefix = "postgresql.datasource")
    public DataSource conciliacionDataSource() {
        return DataSourceBuilder.create().build();
    }

    @Bean
    @Primary
    public LocalContainerEntityManagerFactoryBean conciliacionEntityManagerFactory(
            EntityManagerFactoryBuilder builder) {
        Map<String, Object> properties = new HashMap<>();
        properties.put("hibernate.physical_naming_strategy", namingStrategy);
        properties.put("hibernate.dialect", dialect);
        return builder
                .dataSource(conciliacionDataSource())
                .packages("com.fravega.garexloader.domain.postgresql")
                .persistenceUnit("postgresqlPersistence")
                .properties(properties)
                .build();
    }

    @Bean(name = "postgresqlTransactionManager")
    @Primary
    public JpaTransactionManager conciliacionTransactionManager(
            @Qualifier("postgresqlEntityManagerFactory") final EntityManagerFactory emf) {
        JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(emf);
        return transactionManager;
    }
}
